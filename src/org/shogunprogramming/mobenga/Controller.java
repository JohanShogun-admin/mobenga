/**
 * Copyright 2015 Johan Lohmander
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.shogunprogramming.mobenga;

import java.net.URL;
import java.sql.SQLException;

import org.json.JSONArray;
import org.json.JSONObject;
import org.shogunprogramming.mobenga.model.Player;
import org.shogunprogramming.mobenga.model.PlayerDAO;
import org.shogunprogramming.mobenga.model.PlayerRepository;
import org.shogunprogramming.mobenga.model.database.CsvDatabaseDriver;
import org.shogunprogramming.mobenga.model.database.DatabaseDriver;

/**
 * A Controller to facilitate communication between the view layer (api) and the
 * model layer.
 */
public class Controller {
	private final URL mWebInfFolderLocation;

	/**
	 * Creates a new Controller with a constructor injected URL to the WEB-INF
	 * Folder.
	 *
	 * @param webInfFolder
	 */
	public Controller(final URL webInfFolder) {
		mWebInfFolderLocation = webInfFolder;
	}

	/**
	 * @param playerId
	 *            the Id of the player
	 * @return a json string with the information about the player or an error
	 *         message.
	 */
	public String getPlayerInformation(final String playerId) {
		String returnJson = null;
		if (playerId != null) {
			final Player player = getPlayerForId(playerId);
			if (player != null) {
				returnJson = player.getPlayerInformationJson().toString();
			}
		}

		if (returnJson == null) {
			returnJson = getErrorMessage();
		}
		return returnJson;
	}

	/**
	 * @return a json string with a summary of the players.
	 */
	public String getSummaryPlayerList() {
		String returnJson = null;

		final PlayerRepository playerRepo = PlayerRepository
				.createSummaryRepository(createDatabaseDriver());
		final JSONArray jsonRep = playerRepo != null ? playerRepo
				.getJsonRepresentation() : null;

		if (jsonRep != null) {
			returnJson = jsonRep.toString();
		} else {
			returnJson = getErrorMessage();
		}

		return returnJson;
	}

	// TODO find a better place for this method to live in.
	private Player getPlayerForId(final String id) {
		Player returnPlayer = null;
		final DatabaseDriver databaseDriver = createDatabaseDriver();
		final PlayerDAO playerDAO = new PlayerDAO();
		try {
			returnPlayer = playerDAO.getDetailedPlayerInformation(
					databaseDriver, id);
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		} finally {
			databaseDriver.closeConnection();
		}
		return returnPlayer;
	}

	// TODO move to into an error handling class
	private String getErrorMessage() {
		final JSONObject errorInformation = new JSONObject();
		errorInformation.put("errorMessage",
				"An internal error occured while fetching your information.");
		return errorInformation.toString();
	}

	private DatabaseDriver createDatabaseDriver() {
		return new CsvDatabaseDriver(mWebInfFolderLocation);
	}

}
