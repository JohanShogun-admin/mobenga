/**
 * Copyright 2015 Johan Lohmander
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.shogunprogramming.mobenga.model;

import java.sql.SQLException;
import java.util.List;

import org.json.JSONArray;
import org.shogunprogramming.mobenga.model.database.DatabaseDriver;

/**
 * Class responsible for keeping track of multiple players that are connected
 * for some reason or other.
 */
public class PlayerRepository {
	private final List<Player> mPlayers;

	/**
	 * Creates a new PlayerRepository with constructor injected players.
	 *
	 * @param players
	 *            the players this repository knows of.
	 */
	public PlayerRepository(List<Player> players) {
		mPlayers = players;
	}

	/**
	 * @return a Json array containing all the players json representations.
	 */
	public JSONArray getJsonRepresentation() {
		final JSONArray listOfPlayers = new JSONArray();
		for (Player player : mPlayers) {
			listOfPlayers.put(player.getShortJsonRepresentation());
		}
		return listOfPlayers;
	}

	/**
	 * Creates a new summary repository, fetched from the database.
	 *
	 * @param databaseDriver the driver for the database.
	 * @return a new PlayerRepository
	 */
	public static PlayerRepository createSummaryRepository(final DatabaseDriver databaseDriver) {
		PlayerRepository returnRepository = null;
		final PlayerDAO playerDAO = new PlayerDAO();
		try {
			final List<Player> players = playerDAO
					.getSummaryList(databaseDriver);
			returnRepository = new PlayerRepository(players);
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		} finally {
			databaseDriver.closeConnection();
		}

		return returnRepository;
	}

}
