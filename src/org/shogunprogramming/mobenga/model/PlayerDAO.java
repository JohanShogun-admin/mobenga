/**
 * Copyright 2015 Johan Lohmander
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.shogunprogramming.mobenga.model;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.shogunprogramming.mobenga.model.database.DatabaseDriver;

/**
 * Data access object for creating players.
 */
public class PlayerDAO {
	private static final String LIST_INFORMATION = "SELECT ranking, seasonName, tournamentName,"
			+ "firstName, lastName, playerId FROM players;";

	private static final String PLAYER_INFORMATION = "SELECT ranking, seasonName, tournamentName,"
			+ "firstName, lastName, playerId, teamName, playedPositions, age, height, weight,"
			+ "minsPlayed, assistTotal, yellowCard, redCard, shotsPerGame, aerialWonPerGame,"
			+ "manOfTheMatch, passSuccess FROM players WHERE playerId = ?";

	/**
	 * Returns detailed information about the supplied player.
	 *
	 * @param driver the database driver
	 * @param playerId the player id, unfortunately a string for the database we use.
	 * @return a Player object with a moderate amount of information or null if none was found.
	 * @throws ClassNotFoundException on database error.
	 * @throws SQLException on database error.
	 */
	public Player getDetailedPlayerInformation(final DatabaseDriver driver,
			final String playerId) throws ClassNotFoundException, SQLException {
		driver.initConnection();
		Player player = null;

		final PreparedStatement stmt = driver
				.createPreparedStatement(PLAYER_INFORMATION);
		stmt.setString(1, playerId);
		final ResultSet result = stmt.executeQuery();
		if (result != null && result.next()) {
			player = new Player();
			player.setRanking(result.getInt("ranking"));
			player.setSeasonName(result.getString("seasonName"));
			player.setTournamentName(result.getString("tournamentName"));
			player.setFirstName(result.getString("firstName"));
			player.setLastName(result.getString("lastName"));
			player.setPlayerId(result.getInt("playerId"));
			player.setTeamName(result.getString("teamName"));
			player.setPlayedPositions(result.getString("playedPositions"));
			player.setAge(result.getInt("age"));
			player.setHeight(result.getInt("height"));
			player.setWeight(result.getInt("weight"));
			player.setMinsPlayed(result.getInt("minsPlayed"));
			player.setAssistTotal(result.getInt("assistTotal"));
			player.setYellowCard(result.getInt("yellowCard"));
			player.setRedCard(result.getInt("redCard"));
			player.setShotsPerGame(result.getInt("shotsPerGame"));
			player.setAerialWonPerGame(result.getInt("aerialWonPerGame"));
			player.setManOfTheMatch(result.getInt("manOfTheMatch"));
			player.setPassSuccess(result.getInt("passSuccess"));
		}
		return player;
	}

	/**
	 * Gets a short summary list of all the players.
	 *
	 * @param driver
	 *            the database driver containing the information.
	 * @return a list with players.
	 * @throws ClassNotFoundException
	 *             on database error.
	 * @throws SQLException
	 *             on database error.
	 */
	public ArrayList<Player> getSummaryList(final DatabaseDriver driver)
			throws ClassNotFoundException, SQLException {
		driver.initConnection();

		final ArrayList<Player> players = new ArrayList<>();
		final PreparedStatement stmt = driver
				.createPreparedStatement(LIST_INFORMATION);
		final ResultSet result = stmt.executeQuery();
		while (result != null && result.next()) {
			final Player player = new Player();
			player.setRanking(result.getInt("ranking"));
			player.setSeasonName(result.getString("seasonName"));
			player.setTournamentName(result.getString("tournamentName"));
			player.setFirstName(result.getString("firstName"));
			player.setLastName(result.getString("lastName"));
			player.setPlayerId(result.getInt("playerId"));
			players.add(player);
		}
		result.close();
		stmt.close();

		return players;
	}
}
