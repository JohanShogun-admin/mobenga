/**
 * Copyright 2015 Johan Lohmander
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.shogunprogramming.mobenga.model;

import org.json.JSONObject;

/**
 * Class holding information about a soccer players.
 */
public class Player {
	private int mRanking;
	private int mSeasonId;
	private String mSeasonName;
	private int mTournamentId;
	private String mTournamentRegionId;
	private String mTournamentRegionCode;
	private String mRegionCode;
	private String mTournamentName;
	private String mTournamentShortName;
	private String mFirstName;
	private String mLastName;
	private int mPlayerId;
	private boolean mIsActive;
	private boolean mIsOpta;
	private String mTeamId;
	private String mTeamName;
	private String mPlayedPositions;
	private int mAge;
	private int mHeight;
	private int mWeight;
	private String mPositionText;
	private int mApps;
	private int mSubOn;
	private int mMinsPlayed;
	private int mRating;
	private int mGoal;
	private int mAssistTotal;
	private int mYellowCard;
	private int mRedCard;
	private int mShotsPerGame;
	private int mAerialWonPerGame;
	private int mManOfTheMatch;
	private String mName;
	private boolean mIsManOfTheMatch;
	private double mPlayedPositionsShort;
	private int mPassSuccess;

	/**
	 * Constructor for a player, explicitly declared to indicate that
	 * constructor injection is not used for this class as the data needs
	 * normalization.
	 */
	public Player() {
	}

	public int getRanking() {
		return mRanking;
	}

	public void setRanking(int ranking) {
		mRanking = ranking;
	}

	public int getSeasonId() {
		return mSeasonId;
	}

	public void setSeasonId(int seasonId) {
		mSeasonId = seasonId;
	}

	public String getSeasonName() {
		return mSeasonName;
	}

	public void setSeasonName(String seasonName) {
		mSeasonName = seasonName;
	}

	public int getTournamentId() {
		return mTournamentId;
	}

	public void setTournamentId(int tournamentId) {
		mTournamentId = tournamentId;
	}

	public String getTournamentRegionId() {
		return mTournamentRegionId;
	}

	public void setTournamentRegionId(String tournamentRegionId) {
		mTournamentRegionId = tournamentRegionId;
	}

	public String getTournamentRegionCode() {
		return mTournamentRegionCode;
	}

	public void setTournamentRegionCode(String tournamentRegionCode) {
		mTournamentRegionCode = tournamentRegionCode;
	}

	public String getRegionCode() {
		return mRegionCode;
	}

	public void setRegionCode(String regionCode) {
		mRegionCode = regionCode;
	}

	public String getTournamentName() {
		return mTournamentName;
	}

	public void setTournamentName(String tournamentName) {
		mTournamentName = tournamentName;
	}

	public String getTournamentShortName() {
		return mTournamentShortName;
	}

	public void setTournamentShortName(String tournamentShortName) {
		mTournamentShortName = tournamentShortName;
	}

	public String getFirstName() {
		return mFirstName;
	}

	public void setFirstName(String firstName) {
		mFirstName = firstName;
	}

	public String getLastName() {
		return mLastName;
	}

	public void setLastName(String lastName) {
		mLastName = lastName;
	}

	public int getPlayerId() {
		return mPlayerId;
	}

	public void setPlayerId(int playerId) {
		mPlayerId = playerId;
	}

	public boolean isIsActive() {
		return mIsActive;
	}

	public void setIsActive(boolean isActive) {
		mIsActive = isActive;
	}

	public boolean isOpta() {
		return mIsOpta;
	}

	public void setIsOpta(boolean isOpta) {
		mIsOpta = isOpta;
	}

	public String getTeamId() {
		return mTeamId;
	}

	public void setTeamId(String teamId) {
		mTeamId = teamId;
	}

	public String getTeamName() {
		return mTeamName;
	}

	public void setTeamName(String teamName) {
		mTeamName = teamName;
	}

	public int getAge() {
		return mAge;
	}

	public void setAge(int age) {
		mAge = age;
	}

	public int getHeight() {
		return mHeight;
	}

	public void setHeight(int height) {
		mHeight = height;
	}

	public String getPlayedPositions() {
		return mPlayedPositions;
	}

	public void setPlayedPositions(String playedPositions) {
		mPlayedPositions = playedPositions;
	}

	public int getWeight() {
		return mWeight;
	}

	public void setWeight(int weight) {
		mWeight = weight;
	}

	public String getPositionText() {
		return mPositionText;
	}

	public void setPositionText(String positionText) {
		mPositionText = positionText;
	}

	public int getApps() {
		return mApps;
	}

	public void setApps(int apps) {
		mApps = apps;
	}

	public int getSubOn() {
		return mSubOn;
	}

	public void setSubOn(int subOn) {
		mSubOn = subOn;
	}

	public int getMinsPlayed() {
		return mMinsPlayed;
	}

	public void setMinsPlayed(int minsPlayed) {
		mMinsPlayed = minsPlayed;
	}

	public int getRating() {
		return mRating;
	}

	public void setRating(int rating) {
		mRating = rating;
	}

	public int getGoal() {
		return mGoal;
	}

	public void setGoal(int goal) {
		mGoal = goal;
	}

	public int getAssistTotal() {
		return mAssistTotal;
	}

	public void setAssistTotal(int assistTotal) {
		mAssistTotal = assistTotal;
	}

	public int getYellowCard() {
		return mYellowCard;
	}

	public void setYellowCard(int yellowCard) {
		mYellowCard = yellowCard;
	}

	public int getRedCard() {
		return mRedCard;
	}

	public void setRedCard(int redCard) {
		mRedCard = redCard;
	}

	public int getShotsPerGame() {
		return mShotsPerGame;
	}

	public void setShotsPerGame(int shotsPerGame) {
		mShotsPerGame = shotsPerGame;
	}

	public int getAerialWonPerGame() {
		return mAerialWonPerGame;
	}

	public void setAerialWonPerGame(int aerialWonPerGame) {
		mAerialWonPerGame = aerialWonPerGame;
	}

	public int getManOfTheMatch() {
		return mManOfTheMatch;
	}

	public void setManOfTheMatch(int manOfTheMatch) {
		mManOfTheMatch = manOfTheMatch;
	}

	public String getName() {
		return mName;
	}

	public void setName(String name) {
		mName = name;
	}

	public boolean isIsManOfTheMatch() {
		return mIsManOfTheMatch;
	}

	public void setIsManOfTheMatch(boolean isManOfTheMatch) {
		mIsManOfTheMatch = isManOfTheMatch;
	}

	public double getPlayedPositionsShort() {
		return mPlayedPositionsShort;
	}

	public void setPlayedPositionsShort(double playedPositionsShort) {
		mPlayedPositionsShort = playedPositionsShort;
	}

	public int getPassSuccess() {
		return mPassSuccess;
	}

	public void setPassSuccess(int passSuccess) {
		mPassSuccess = passSuccess;
	}

	/**
	 * @return A JSON object with the interesting data for a short representation.
	 */
	public JSONObject getShortJsonRepresentation() {
		final JSONObject json = new JSONObject ();
		json.put("ranking", mRanking);
		json.put("seasonName", mSeasonName);
		json.put("tournamentName", mTournamentName);
		json.put("firstName", mFirstName);
		json.put("lastName", mLastName);
		json.put("playerId", mPlayerId);
		return json;
	}

	/**
	 * @return A JSON object with the interesting data for a more informed representation.
	 */
	public JSONObject getPlayerInformationJson() {
		final JSONObject json = new JSONObject ();
		json.put("ranking", mRanking);
		json.put("seasonName", mSeasonName);
		json.put("tournamentName", mTournamentName);
		json.put("firstName", mFirstName);
		json.put("lastName", mLastName);
		json.put("playerId", mPlayerId);
		json.put("teamName", mTeamName);
		json.put("playedPositions", mPlayedPositions);
		json.put("age", mAge);
		json.put("height", mHeight);
		json.put("weight", mWeight);
		json.put("minsPlayed", mMinsPlayed);
		json.put("assistTotal", mAssistTotal);
		json.put("yellowCard", mYellowCard);
		json.put("redCard", mRedCard);
		json.put("shotsPerGame", mShotsPerGame);
		json.put("aerialWonPerGame", mAerialWonPerGame);
		json.put("manOfTheMatch", mManOfTheMatch);
		json.put("passSuccess", mPassSuccess);
		json.put("passSuccess", mPassSuccess);
		return json;
	}

}
