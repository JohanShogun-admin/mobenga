/**
 * Copyright 2015 Johan Lohmander
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.shogunprogramming.mobenga.model.database;

import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * Class responsible for creating and manage Csv databases.
 */
public class CsvDatabaseDriver implements DatabaseDriver {

	/**
	 * The database connection.
	 */
	private Connection mConnection;

	/**
	 * Used in order to be able to access the csv file used as the database.
	 */
	private URL mResourcePath;

	/**
	 * Constructor with injected dependency to the path where the resources for
	 * the csv database is located.
	 *
	 * @param resourcePath
	 *            the path to the resources.
	 */
	public CsvDatabaseDriver(final URL resourcePath) {
		mResourcePath = resourcePath;
	}

	@Override
	public Connection initConnection() throws SQLException,
			ClassNotFoundException {
		if (mConnection == null) {
			Class.forName("org.relique.jdbc.csv.CsvDriver");
			mConnection = DriverManager.getConnection("jdbc:relique:csv:"
					+ mResourcePath.getPath());
		}
		return mConnection;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public PreparedStatement createPreparedStatement(final String query) throws SQLException {
		PreparedStatement returnStatement = null;
		if (mConnection != null && !mConnection.isClosed()) {
			returnStatement = mConnection.prepareStatement(query);
		}
		return returnStatement;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void closeConnection() {
		try {
			if (mConnection != null && !mConnection.isClosed()) {
				mConnection.close();
			}
		} catch (SQLException e) {
		}
	}

}
