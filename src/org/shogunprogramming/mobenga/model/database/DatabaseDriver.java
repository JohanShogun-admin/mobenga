/**
 * Copyright 2015 Johan Lohmander
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.shogunprogramming.mobenga.model.database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * Interface creating an abstraction around database drivers allowing for an
 * easy exchange in the future.
 */
public interface DatabaseDriver {

	/**
	 * @return A database connection ready for use.
	 * @throws SQLException
	 *             on SQL error.
	 * @throws ClassNotFoundException
	 *             on SQL driver not found.
	 */
	Connection initConnection() throws SQLException, ClassNotFoundException;

	/**
	 * @return An SQL statement ready for use.
	 * @throws SQLException
	 *             on SQL error.
	 */
	PreparedStatement createPreparedStatement(final String query) throws SQLException;

	void closeConnection();

}
