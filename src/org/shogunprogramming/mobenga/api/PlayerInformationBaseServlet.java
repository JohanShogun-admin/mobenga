/**
 * Copyright 2015 Johan Lohmander
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.shogunprogramming.mobenga.api;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.shogunprogramming.mobenga.Controller;

/**
 * Base servlet for providing player information.
 */
public abstract class PlayerInformationBaseServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final String WEB_INF_FOLDER_NAME = "/WEB-INF/";

	protected Controller mController;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(final HttpServletRequest request,
			final HttpServletResponse response) throws ServletException,
			IOException {
		processRequest(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(final HttpServletRequest request,
			final HttpServletResponse response) throws ServletException,
			IOException {
		processRequest(request, response);
	}

	private void processRequest(final HttpServletRequest request,
			final HttpServletResponse response) throws IOException {
		// Init Controller for requests
		mController = new Controller(getServletContext().getResource(
				WEB_INF_FOLDER_NAME));

		// Print information from our sub classes.
		response.setCharacterEncoding("UTF-8");
		final String responseData = getDisplayInformation(request);
		final PrintWriter writer = response.getWriter();
		writer.write(new String (responseData.getBytes("UTF-8")));
		writer.flush();
		writer.close();
	}

	protected abstract String getDisplayInformation(
			final HttpServletRequest request);
}
