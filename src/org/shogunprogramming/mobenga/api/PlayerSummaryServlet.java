/**
 * Copyright 2015 Johan Lohmander
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.shogunprogramming.mobenga.api;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;

/**
 * Http Serverlet for providing information about players. How the API is made
 * is available in:
 */
@WebServlet("/api/players")
public class PlayerSummaryServlet extends PlayerInformationBaseServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected String getDisplayInformation(HttpServletRequest request) {
		return mController.getSummaryPlayerList();
	}

}
