/**
 * Copyright 2015 Johan Lohmander
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

// Ensure we're using strict mode if available,
// reduces issues and errors.
"use strict";

// Protocol to use,
// I wish we could have gone with "gohper", if nothing else for the name.
var kPROTOCOL = "http://";
// Name of the host
var kHOST_NAME = "localhost";
// Port to connect to
var kPORT = ":8080";
// Name of the server
var kSERVER_NAME = "/MobengaBackEnd";

// Path to the api on the server.
var kAPI_LOCATION = "/api";
// List players end point.
var kLIST_PLAYERS_END_POINT = kAPI_LOCATION + "/players";
// Player information end point.
var kPLAYER_INFORMATION_END_POINT = kAPI_LOCATION + "/player/"

/**
 * Returns the URL to the server.
 */
function api_getServerUrl() {
	return kPROTOCOL + kHOST_NAME + kPORT + kSERVER_NAME;
}

/**
 * Returns the URL for the list players end point.
 */
function api_getListPlayersUrl() {
	return api_getServerUrl() + kLIST_PLAYERS_END_POINT;
}

/**
 * Returns the URL for the end point for the supplied player id.
 */
function api_getPlayerInformation(playerId) {
	return api_getServerUrl() + kPLAYER_INFORMATION_END_POINT + playerId;
}
