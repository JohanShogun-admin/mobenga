/**
 * Copyright 2015 Johan Lohmander
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

// Ensure we're using strict mode if available,
// reduces issues and errors.
"use strict";

// Init the entire schinding.
pyr_loadPlayerList();

/**
 * Loads the player list and inserts it on the web page.
 */
function pyr_loadPlayerList() {
	var xmlHttp = new XMLHttpRequest();
	xmlHttp.onreadystatechange = function() {
		if (xmlHttp.readyState == 4 && xmlHttp.status == 200) {
			var playerList = JSON.parse(xmlHttp.responseText);
			pyr_constructPlayerListView(playerList);
		}
	}

	// Lets make an async request.
	xmlHttp.open("GET", api_getListPlayersUrl(), true);
	xmlHttp.send();
}

function pyr_loadPlayerInformation(playerId) {
	var xmlHttp = new XMLHttpRequest();
	xmlHttp.onreadystatechange = function() {
		if (xmlHttp.readyState == 4 && xmlHttp.status == 200) {
			var playerInfoJson = JSON.parse(xmlHttp.responseText);
			pyr_constructHeadsUpPlayerView(playerInfoJson);
		}
	}

	// Lets make an synchronous request.
	xmlHttp.open("GET", api_getPlayerInformation(playerId), false);
	xmlHttp.send();
}

/**
 * Constructs the players list view and attaches it to the document.
 */
function pyr_constructPlayerListView(playerJsonList) {
	var playerListView = document.getElementById("playerList");
	pyr_clearAllChildren(playerListView);
	playerJsonList.forEach(function(playerJson) {
		var row = document.createElement("tr");
		pyr_populatePlayerListView(playerJson, row);
		playerListView.appendChild(row);
	});
	pyr_showListView();
	pyr_hideBackButton();
}

function pyr_hideListView() {
	var playerInformationView = document.getElementById("playerListContainer");
	playerInformationView.style.display = "none";
}

function pyr_showListView() {
	var playerInformationView = document.getElementById("playerListContainer");
	playerInformationView.style.display = "inline-block";
}

function pyr_showBackButton() {
	var backButtonView = document.getElementById("backButtonContainer");
	backButtonView.style.display = "block";
}

function pyr_hideBackButton() {
	var backButtonView = document.getElementById("backButtonContainer");
	backButtonView.style.display = "none";
}

function pyr_clearHeadsUpPlayerView() {
	var playerInformationView = document.getElementById("playerInformation");
	playerInformationView.style.display = "none";
}

function pyr_clearAllChildren(element) {
	while (element.firstChild) {
		element.removeChild(element.firstChild);
	}
}

function pyr_populatePlayerListView(playerJson, row) {

	// Unfortunately the support for HTML imports is only available
	// in Chrome and Opera at the point of writing this,
	// so falling back on old school element creation.
	var rankingData = document.createElement("td");
	var rankingText = document.createTextNode(playerJson.ranking);
	rankingData.appendChild(rankingText);
	row.appendChild(rankingData);

	var playerNameData = document.createElement("td");
	var playerNameText = document.createTextNode(playerJson.firstName + " "
			+ playerJson.lastName);
	playerNameData.appendChild(playerNameText);
	row.appendChild(playerNameData);

	var tournamentNameData = document.createElement("td");
	var tournamentName = document.createTextNode(playerJson.tournamentName);
	tournamentNameData.appendChild(tournamentName);
	row.appendChild(tournamentNameData);

	var seasonData = document.createElement("td");
	var season = document.createTextNode(playerJson.seasonName);
	seasonData.appendChild(season);
	row.appendChild(seasonData);

	row.onclick = function() {
		pyr_hideListView();
		pyr_showBackButton();
		pyr_loadPlayerInformation(playerJson.playerId);
	}
	row.onmouseover = function() {
		pyr_loadPlayerInformation(playerJson.playerId);
	}
}

function pyr_constructHeadsUpPlayerView(playerInfoJson) {
	// Set player name
	var view = document.getElementById("morePlayerInfoName");
	var text = document.createTextNode(playerInfoJson.firstName + " "
			+ playerInfoJson.lastName);
	pyr_clearAllChildren(view);
	view.appendChild(text);

	view = document.getElementById("morePlayerInfoTeamName");
	text = document.createTextNode(playerInfoJson.teamName);
	pyr_clearAllChildren(view);
	view.appendChild(text);

	view = document.getElementById("morePlayerInfoRedCard");
	text = document.createTextNode(playerInfoJson.redCard);
	pyr_clearAllChildren(view);
	view.appendChild(text);

	view = document.getElementById("morePlayerInfoYellowCard");
	text = document.createTextNode(playerInfoJson.yellowCard);
	pyr_clearAllChildren(view);
	view.appendChild(text);

	view = document.getElementById("morePlayerInfoAerialWonPerGame");
	text = document.createTextNode(playerInfoJson.aerialWonPerGame);
	pyr_clearAllChildren(view);
	view.appendChild(text);

	view = document.getElementById("morePlayerInfoPassSuccess");
	text = document.createTextNode(playerInfoJson.passSuccess);
	pyr_clearAllChildren(view);
	view.appendChild(text);

	view = document.getElementById("morePlayerInfoWeight");
	text = document.createTextNode(playerInfoJson.weight);
	pyr_clearAllChildren(view);
	view.appendChild(text);

	view = document.getElementById("morePlayerInfoAssistTotal");
	text = document.createTextNode(playerInfoJson.assistTotal);
	pyr_clearAllChildren(view);
	view.appendChild(text);

	view = document.getElementById("morePlayerInfoWeight");
	text = document.createTextNode(playerInfoJson.weight);
	pyr_clearAllChildren(view);
	view.appendChild(text);

	view = document.getElementById("morePlayerInfoAssistTotal");
	text = document.createTextNode(playerInfoJson.assistTotal);
	pyr_clearAllChildren(view);
	view.appendChild(text);

	view = document.getElementById("morePlayerInfoTournamentName");
	text = document.createTextNode(playerInfoJson.tournamentName);
	pyr_clearAllChildren(view);
	view.appendChild(text);

	view = document.getElementById("morePlayerInfoSeasonName");
	text = document.createTextNode(playerInfoJson.seasonName);
	pyr_clearAllChildren(view);
	view.appendChild(text);

	view = document.getElementById("morePlayerInfoRanking");
	text = document.createTextNode(playerInfoJson.ranking);
	pyr_clearAllChildren(view);
	view.appendChild(text);

	view = document.getElementById("morePlayerInfoMinsPlayed");
	text = document.createTextNode(playerInfoJson.minsPlayed);
	pyr_clearAllChildren(view);
	view.appendChild(text);

	view = document.getElementById("morePlayerInfoShotsPerGame");
	text = document.createTextNode(playerInfoJson.shotsPerGame);
	pyr_clearAllChildren(view);
	view.appendChild(text);

	view = document.getElementById("morePlayerInfoPlayedPositions");
	text = document.createTextNode(playerInfoJson.playedPositions);
	pyr_clearAllChildren(view);
	view.appendChild(text);

	view = document.getElementById("morePlayerInfoAge");
	text = document.createTextNode(playerInfoJson.age);
	pyr_clearAllChildren(view);
	view.appendChild(text);

	view = document.getElementById("morePlayerInfoManOfTheMatch");
	text = document.createTextNode(playerInfoJson.manOfTheMatch);
	pyr_clearAllChildren(view);
	view.appendChild(text);

	view = document.getElementById("morePlayerInfoHeight");
	text = document.createTextNode(playerInfoJson.height);
	pyr_clearAllChildren(view);
	view.appendChild(text);

	// Finally make it visible.
	var playerInformationView = document.getElementById("playerInformation");
	playerInformationView.style.display = "inline-block";
}
